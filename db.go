package localdb

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"os"
	"time"
)

var LDB *sql.DB

type RequestLog struct {
	Id          int
	DateCreated time.Time
	RouteId     string
	RemoteAddr  string
	Response    int64
}

type DailyCountLog struct {
	Hour  string
	Count int
}

type DailyResponseLog struct {
	Hour     string
	Response float64
}

type RouteCountLog struct {
	Route string
	Count int
}

type RouteResponseLog struct {
	Route    string
	Response float64
}

type GeoLog struct {
	CountryCode string
	Count       int
}

func init() {
	var err error
	host := os.Getenv("POSTGRES_PORT_5432_TCP_ADDR")
	LDB, err = sql.Open("postgres", "host="+host+" user=localpanda dbname=localpanda password=localpanda1234 sslmode=disable")
	schema := `
CREATE TABLE IF NOT EXISTS requests (
	id serial primary key,
	date_created	timestamp default CURRENT_TIMESTAMP,
	routeid     varchar(255) not null,	
	remoteaddr  varchar(255) not null,
	response    int
);
CREATE TABLE IF NOT EXISTS ip2location(
	ip_from bigint NOT NULL,
	ip_to bigint NOT NULL,
	country_code character(2) NOT NULL,
	country_name character varying(64) NOT NULL,
	region_name character varying(128) NOT NULL,
	city_name character varying(128) NOT NULL,
	CONSTRAINT ip2location_pkey PRIMARY KEY (ip_from, ip_to)
);
`
	_, err = LDB.Exec(schema)
	if err != nil {
		fmt.Println("Cannot create table", err)
	}
}

func Add(dateCreated time.Time, routeId string, remoteAddr string, response int64) (err error) {
	_, err = LDB.Exec("insert into requests(date_created, routeid, remoteaddr, response) values ($1, $2, $3, $4);",
		dateCreated, routeId, remoteAddr, response)
	return
}

func All() (logs []RequestLog, err error) {
	rows, err := LDB.Query("select * from requests")
	defer rows.Close()
	for rows.Next() {
		req := RequestLog{}
		err = rows.Scan(&req.Id, &req.DateCreated, &req.RouteId, &req.RemoteAddr, &req.Response)
		logs = append(logs, req)
	}
	err = rows.Err()
	return
}

func ByRoute(routeId string) (logs []RequestLog, err error) {
	rows, err := LDB.Query("select * from requests where routeid = $1", routeId)
	defer rows.Close()
	for rows.Next() {
		req := RequestLog{}
		err = rows.Scan(&req.DateCreated, &req.RouteId, &req.RemoteAddr, &req.Response)
		logs = append(logs, req)
	}
	err = rows.Err()
	return
}

func DailyCountByRoute(routeId string) (logs []DailyCountLog, err error) {
	query := `
with hourly as (
  select 
    (now() - '1 day'::interval) + ( n    || ' hours')::interval start_time,
    (now() - '1 day'::interval) + ((n+1) || ' hours')::interval end_time
  from generate_series(0, 23, 1) n
)
select extract(hour from h.start_time) as hour, count(r.routeid) as count
from (select * from requests where routeid = $1) as r
right join hourly h 
        on r.date_created >= h.start_time and r.date_created < h.end_time
group by h.start_time
order by h.start_time asc;	
`
	rows, err := LDB.Query(query, routeId)
	defer rows.Close()
	for rows.Next() {
		log := DailyCountLog{}
		err = rows.Scan(&log.Hour, &log.Count)
		logs = append(logs, log)
	}
	err = rows.Err()
	return
}

func DailyResponseByRoute(routeId string) (logs []DailyResponseLog, err error) {
	query := `
with hourly as (
  select 
    (now() - '1 day'::interval) + ( n    || ' hours')::interval start_time,
    (now() - '1 day'::interval) + ((n+1) || ' hours')::interval end_time
  from generate_series(0, 23, 1) n
)
select extract(hour from h.start_time) as hour, cast(coalesce(avg(r.response), 0)/1000000 as float) avg_response 
from (select * from requests where routeid = $1) as r
right join hourly h 
        on r.date_created >= h.start_time and r.date_created < h.end_time
group by h.start_time
order by h.start_time asc;	
`
	rows, err := LDB.Query(query, routeId)
	defer rows.Close()
	for rows.Next() {
		log := DailyResponseLog{}
		err = rows.Scan(&log.Hour, &log.Response)
		logs = append(logs, log)
	}
	err = rows.Err()
	return
}

func DailyCount() (logs []RouteCountLog, err error) {
	query := `
select routeid, count(routeid) as count
from requests where date_created > (now() - '1 day'::interval)
group by routeid
order by count asc;	
`
	rows, err := LDB.Query(query)
	defer rows.Close()
	for rows.Next() {
		log := RouteCountLog{}
		err = rows.Scan(&log.Route, &log.Count)
		logs = append(logs, log)
	}
	err = rows.Err()
	return

}

func DailyResponse() (logs []RouteResponseLog, err error) {
	query := `
select routeid, cast(coalesce(avg(response), 0)/1000000 as float) avg_response 
from requests where date_created > (now() - '1 day'::interval)
group by routeid
order by avg_response desc;
`
	rows, err := LDB.Query(query)
	defer rows.Close()
	for rows.Next() {
		log := RouteResponseLog{}
		err = rows.Scan(&log.Route, &log.Response)
		logs = append(logs, log)
	}
	err = rows.Err()
	return
}

func GeoCount() (logs []GeoLog, err error) {
	query := `
select c, country_name 
from (select count(*) as c, (split_part(remoteaddr,':',1)::inet - '0.0.0.0'::inet) as addr from requests group by addr) as reqs, ip2location 
where addr < ip_to and addr > ip_from 
group by reqs.c, country_name;
`
	rows, err := LDB.Query(query)
	defer rows.Close()
	for rows.Next() {
		log := GeoLog{}
		err = rows.Scan(&log.Count, &log.CountryCode)
		logs = append(logs, log)
	}
	err = rows.Err()
	return
}
