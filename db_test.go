package localdb

import (
	"testing"
	// "time"
)

// func TestDB(t *testing.T) {
// 	t0 := time.Now()
// 	time.Sleep(100 * time.Millisecond)
// 	d := time.Since(t0).Nanoseconds()
// 	err := Add(t0, "test", "123", d)
// 	if err != nil {
// 		t.Error("Cannot add", err)
// 	}
// }

func TestSelect(t *testing.T) {
	all, err := All()
	if err != nil {
		t.Error("Cannot get all records", err)
	}
	t.Log(all)
}

func TestCount(t *testing.T) {
	logs, err := DailyCountByRoute("GET/_/test6")
	if err != nil {
		t.Error("Cannot get count", err)
	}
	t.Log(logs)
}